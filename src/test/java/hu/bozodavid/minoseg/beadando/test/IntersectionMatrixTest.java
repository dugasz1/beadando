package hu.bozodavid.minoseg.beadando.test;

import hu.bozodavid.minoseg.beadando.IntersectionMatrix;
import hu.bozodavid.minoseg.beadando.exceptions.InvalidMatrixValue;
import org.junit.Assert;
import org.junit.Test;

public class IntersectionMatrixTest {

    @Test
    public void getSize_getTheMatrixSize_ReturnWithN() throws Exception {
        IntersectionMatrix matrix = new IntersectionMatrix(23);

        Assert.assertEquals(23, matrix.getSize());
    }

    @Test
    public void setGet_Save256_ReturnWithIt() throws Exception {
        IntersectionMatrix matrix = new IntersectionMatrix(3);
        matrix.set(0, 0, 256);

        Assert.assertEquals(256, matrix.get(0, 0));
    }

    @Test
    public void setGet_Save128_ReturnWithIt() throws Exception {
        IntersectionMatrix matrix = new IntersectionMatrix(3);
        matrix.set(2, 2, 128);

        Assert.assertEquals(128, matrix.get(2, 2));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void set_OutOfRange_ThrowsException() throws Exception {
        IntersectionMatrix matrix = new IntersectionMatrix(3);
        matrix.set(6, 6, 128);
    }

    @Test(expected = InvalidMatrixValue.class)
    public void set_NegativeValue_ThrowsException() throws Exception {
        IntersectionMatrix matrix = new IntersectionMatrix(3);
        matrix.set(0, 0, -1);
    }

    @Test
    public void setGetRow_SetRowAt0_ReturnWithSameRow() throws Exception {
        IntersectionMatrix matrix = new IntersectionMatrix(3);

        matrix.setRow(0, new int[]{0, 1, 2});

        Assert.assertArrayEquals(new int[]{0, 1, 2}, matrix.getRow(0));
    }

    @Test
    public void setGetRow_SetRowAt2_ReturnWithSameRow() throws Exception {
        IntersectionMatrix matrix = new IntersectionMatrix(3);

        matrix.setRow(2, new int[]{2, 2, 3});

        Assert.assertArrayEquals(new int[]{2, 2, 3}, matrix.getRow(2));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void setRow_SetRowAtOutOfRange_ThrowsException() throws Exception {
        IntersectionMatrix matrix = new IntersectionMatrix(3);

        matrix.setRow(6, new int[]{2, 2, 3});
    }

    @Test(expected = InvalidMatrixValue.class)
    public void setRow_NegativeValue_ThrowsException() throws Exception {
        IntersectionMatrix matrix = new IntersectionMatrix(3);
        matrix.setRow(0, new int[]{2, -1, 2});
    }

}
