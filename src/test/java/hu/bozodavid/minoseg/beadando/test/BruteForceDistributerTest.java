package hu.bozodavid.minoseg.beadando.test;

import hu.bozodavid.minoseg.beadando.BruteForceDistributer;
import hu.bozodavid.minoseg.beadando.Distributer;
import hu.bozodavid.minoseg.beadando.FileParser;
import hu.bozodavid.minoseg.beadando.IntersectionMatrix;
import org.junit.Assert;
import org.junit.Test;

public class BruteForceDistributerTest {

    @Test
    public void isDistributionContains_ContainsTheNumber_ReturnTrue() {
        BruteForceDistributer distributer = new BruteForceDistributer(null);
        distributer.distribution = new int[]{1, 5, 7, 9};

        Assert.assertEquals(true, distributer.isDistributionContains(7, 4));
    }

    @Test
    public void isDistributionContains_NotContainsTheNumber_ReturnFalse() {
        BruteForceDistributer distributer = new BruteForceDistributer(null);
        distributer.distribution = new int[]{1, 5, 7, 9};

        Assert.assertEquals(false, distributer.isDistributionContains(11, 4));
    }

    @Test
    public void isDistributionContains_ContainsTheNumberInRowLimit_ReturnTrue() {
        BruteForceDistributer distributer = new BruteForceDistributer(null);
        distributer.distribution = new int[]{1, 5, 7, 9};

        Assert.assertEquals(true, distributer.isDistributionContains(5, 2));
    }

    @Test
    public void isDistributionContains_NotContainsTheNumberBecauseRowLimit_ReturnFalse() {
        BruteForceDistributer distributer = new BruteForceDistributer(null);
        distributer.distribution = new int[]{1, 5, 7, 9};

        Assert.assertEquals(false, distributer.isDistributionContains(9, 3));
    }

    @Test
    public void getCurrentMinIntersection_ExtractCorrectMinimum_ReturnMinimumIntersection() throws Exception {
        IntersectionMatrix matrix = new IntersectionMatrix(3);
        matrix.setRow(0, new int[]{1, 2, 3});
        matrix.setRow(1, new int[]{1, 2, 3});
        matrix.setRow(2, new int[]{1, 2, 3});
        BruteForceDistributer distributer = new BruteForceDistributer(matrix);
        distributer.distribution = new int[]{0, 1, 2};

        Assert.assertEquals(1, distributer.getCurrentMinIntersection());
    }

    @Test
    public void getCurrentMinIntersection_ExtractCorrectMinimum2_ReturnMinimumIntersection() throws Exception {
        IntersectionMatrix matrix = new IntersectionMatrix(3);
        matrix.setRow(0, new int[]{1, 2, 3});
        matrix.setRow(1, new int[]{1, 2, 3});
        matrix.setRow(2, new int[]{1, 2, 3});
        BruteForceDistributer distributer = new BruteForceDistributer(matrix);
        distributer.distribution = new int[]{1, 1, 2};

        Assert.assertEquals(2, distributer.getCurrentMinIntersection());
    }

    @Test
    public void getCurrentMinIntersection_ExtractCorrectMinimum3_ReturnMinimumIntersection() throws Exception {
        IntersectionMatrix matrix = new IntersectionMatrix(3);
        matrix.setRow(0, new int[]{1, 2, 3});
        matrix.setRow(1, new int[]{1, 2, 3});
        matrix.setRow(2, new int[]{1, 2, 3});
        BruteForceDistributer distributer = new BruteForceDistributer(matrix);
        distributer.distribution = new int[]{2, 2, 2};

        Assert.assertEquals(3, distributer.getCurrentMinIntersection());
    }

    @Test
    public void distribute_Valid3x3Matrix_Return3() throws Exception {
        IntersectionMatrix matrix = new IntersectionMatrix(3);
        matrix.setRow(0, new int[]{5, 2, 2});
        matrix.setRow(1, new int[]{2, 3, 5});
        matrix.setRow(2, new int[]{2, 1, 3});

        Distributer distributer = new BruteForceDistributer(matrix);
        distributer.distribute();

        Assert.assertEquals(3, distributer.getMinIntersection());
    }

    @Test
    public void distribution_Valid4x4Matrix_Return5() throws Exception {
        IntersectionMatrix matrix = new IntersectionMatrix(4);
        matrix.setRow(0, new int[]{5, 2, 2, 8});
        matrix.setRow(1, new int[]{2, 3, 5, 8});
        matrix.setRow(2, new int[]{2, 1, 3, 8});
        matrix.setRow(3, new int[]{8, 8, 8, 8});

        Distributer distributer = new BruteForceDistributer(matrix);
        distributer.distribute();

        Assert.assertEquals(5, distributer.getMinIntersection());
    }


    @Test
    public void distribution_Valid5x5Matrix_Return3() throws Exception {
        IntersectionMatrix matrix = new IntersectionMatrix(5);
        matrix.setRow(0, new int[]{5, 2, 2, 1, 4});
        matrix.setRow(1, new int[]{2, 3, 5, 2, 3});
        matrix.setRow(2, new int[]{2, 1, 3, 5, 6});
        matrix.setRow(3, new int[]{2, 1, 3, 2, 5});
        matrix.setRow(4, new int[]{2, 1, 3, 2, 5});

        Distributer distributer = new BruteForceDistributer(matrix);
        distributer.distribute();

        Assert.assertEquals(3, distributer.getMinIntersection());
    }

    @Test
    public void getRunCount_Valid3x3Matrix_Return6() throws Exception {
        IntersectionMatrix matrix = new IntersectionMatrix(3);
        matrix.setRow(0, new int[]{5, 2, 2});
        matrix.setRow(1, new int[]{2, 3, 5});
        matrix.setRow(2, new int[]{2, 1, 3});

        Distributer distributer = new BruteForceDistributer(matrix);
        distributer.distribute();

        // 6 = 3!
        Assert.assertEquals(6, distributer.getRunCount());
    }

    @Test
    public void getRunCount_Valid4x4Matrix_Return24() throws Exception {
        IntersectionMatrix matrix = new IntersectionMatrix(4);
        matrix.setRow(0, new int[]{5, 2, 2, 8});
        matrix.setRow(1, new int[]{2, 3, 5, 8});
        matrix.setRow(2, new int[]{2, 1, 3, 8});
        matrix.setRow(3, new int[]{8, 8, 8, 8});

        Distributer distributer = new BruteForceDistributer(matrix);
        distributer.distribute();

        // 24 = 4!
        Assert.assertEquals(24, distributer.getRunCount());
    }


    @Test
    public void getRunCount_Valid5x5Matrix_Return120() throws Exception {
        IntersectionMatrix matrix = new IntersectionMatrix(5);
        matrix.setRow(0, new int[]{5, 2, 2, 1, 4});
        matrix.setRow(1, new int[]{2, 3, 5, 2, 3});
        matrix.setRow(2, new int[]{2, 1, 3, 5, 6});
        matrix.setRow(3, new int[]{2, 1, 3, 2, 5});
        matrix.setRow(4, new int[]{2, 1, 3, 2, 5});

        Distributer distributer = new BruteForceDistributer(matrix);
        distributer.distribute();

        // 120 = 5!
        Assert.assertEquals(120, distributer.getRunCount());
    }


}
