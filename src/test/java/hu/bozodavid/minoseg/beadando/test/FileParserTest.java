package hu.bozodavid.minoseg.beadando.test;


import hu.bozodavid.minoseg.beadando.FileParser;
import hu.bozodavid.minoseg.beadando.IntersectionMatrix;
import hu.bozodavid.minoseg.beadando.exceptions.InvalidInputFile;
import org.junit.Assert;
import org.junit.Test;

import java.io.InputStream;

public class FileParserTest {

    @Test(expected = InvalidInputFile.class)
    public void parse_EmptyInput_ThrowsException() throws Exception {
        FileParser fileParser = new FileParser();
        InputStream stream = this.getClass().getClassLoader().getResourceAsStream("empty.txt");

        fileParser.parse(stream);
    }

    @Test(expected = InvalidInputFile.class)
    public void parse_FirstLineMissing_ThrowsException() throws Exception {
        FileParser fileParser = new FileParser();
        InputStream stream = this.getClass().getClassLoader().getResourceAsStream("firstLineMissing.txt");

        fileParser.parse(stream);
    }

    @Test(expected = InvalidInputFile.class)
    public void parse_FirstLineInvalidCharacter_ThrowsException() throws Exception {
        FileParser fileParser = new FileParser();
        InputStream stream = this.getClass().getClassLoader().getResourceAsStream("firstLineInvalid.txt");

        fileParser.parse(stream);
    }

    @Test(expected = InvalidInputFile.class)
    public void parse_NotEnoughRow_ThrowsException() throws Exception {
        FileParser fileParser = new FileParser();
        InputStream stream = this.getClass().getClassLoader().getResourceAsStream("notEnoughRow.txt");

        fileParser.parse(stream);
    }

    @Test(expected = InvalidInputFile.class)
    public void parse_MissingColumn_ThrowsException() throws Exception {
        FileParser fileParser = new FileParser();
        InputStream stream = this.getClass().getClassLoader().getResourceAsStream("missingColumn.txt");

        fileParser.parse(stream);
    }

    @Test
    public void parse_GoodInput_Pass() throws Exception {
        FileParser fileParser = new FileParser();
        InputStream stream = this.getClass().getClassLoader().getResourceAsStream("valid3x3_3.txt");

        IntersectionMatrix matrix = fileParser.parse(stream);

        Assert.assertEquals(matrix.get(0, 0), 5);
        Assert.assertEquals(matrix.get(0, 1), 2);
        Assert.assertEquals(matrix.get(0, 2), 2);
        Assert.assertEquals(matrix.get(1, 0), 2);
        Assert.assertEquals(matrix.get(1, 1), 3);
        Assert.assertEquals(matrix.get(1, 2), 5);
        Assert.assertEquals(matrix.get(2, 0), 2);
        Assert.assertEquals(matrix.get(2, 1), 1);
        Assert.assertEquals(matrix.get(2, 2), 3);
    }
}
