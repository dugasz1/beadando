package hu.bozodavid.minoseg.beadando;

import java.util.Arrays;
import java.util.stream.IntStream;

public class BruteForceDistributer implements Distributer {
    private IntersectionMatrix matrix;
    /**
     * index is the row (hunting field)
     * value is the column (farm)
     */
    public int[] distribution;

    private int runCounter;

    private int minIntersectionOfDistribution;

    public BruteForceDistributer(IntersectionMatrix matrix) {
        this.matrix = matrix;
        minIntersectionOfDistribution = 0;
    }

    public void iterateRowRecursive(int row) {
        if (row == matrix.getSize() - 1) {
            iterateLastRow(row);
        } else {
            iterateNotLastRow(row);
        }
    }

    private void iterateLastRow(int row) {
        for (int i = 0; i < matrix.getSize(); i++) {
            if (isDistributionContains(i, row)) {
                continue;
            }

            distribution[row] = i;

            int currentMin = getCurrentMinIntersection();
            if (currentMin > minIntersectionOfDistribution) {
                minIntersectionOfDistribution = currentMin;
            }

            runCounter++;
        }
    }

    private void iterateNotLastRow(int row) {
        for (int i = 0; i < matrix.getSize(); i++) {
            if (isDistributionContains(i, row)) {
                continue;
            }

            distribution[row] = i;
            iterateRowRecursive(row + 1);
        }
    }

    public boolean isDistributionContains(int number, int row) {
        for (int i = 0; i < row; i++) {
            if (number == distribution[i]) {
                return true;
            }
        }

        return false;
    }

    public int getCurrentMinIntersection() {
        int min = matrix.get(0, distribution[0]);
        for (int i = 0; i < distribution.length; i++) {
            int intersection = matrix.get(i, distribution[i]);
            if (min > intersection) {
                min = intersection;
            }
        }

        return min;
    }

    @Override
    public void distribute() {
        runCounter = 0;
        distribution = new int[matrix.getSize()];
        Arrays.fill(distribution, -1);

        iterateRowRecursive(0);
    }

    @Override
    public int getMinIntersection() {
        return minIntersectionOfDistribution;
    }

    @Override
    public int getRunCount() {
        return runCounter;
    }
}
