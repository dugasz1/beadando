package hu.bozodavid.minoseg.beadando;

import hu.bozodavid.minoseg.beadando.exceptions.InvalidInputFile;
import hu.bozodavid.minoseg.beadando.exceptions.InvalidMatrixValue;
import org.omg.CORBA.DynAnyPackage.Invalid;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

public class FileParser {
    public IntersectionMatrix parse(InputStream stream) throws InvalidInputFile, IOException, InvalidMatrixValue {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

        int n = parseFirstLine(reader);

        return parseIntersectionData(reader, n);
    }

    private IntersectionMatrix parseIntersectionData(BufferedReader reader, int n) throws IOException, InvalidInputFile, InvalidMatrixValue {
        IntersectionMatrix matrix = new IntersectionMatrix(n);
        for (int i = 0; i < n; i++) {
            String line = reader.readLine();

            if (line == null) {
                throw new InvalidInputFile("Not enought row.");
            }

            String[] numbers = line.split("\\s+");
            if (numbers.length != n) {
                throw new InvalidInputFile("Column missing.");
            }

            int[] row = Arrays.stream(numbers).mapToInt(Integer::parseInt).toArray();
            matrix.setRow(i, row);
        }
        return matrix;
    }

    public int parseFirstLine(BufferedReader scanner) throws InvalidInputFile, IOException {
        int numberOfCouples;
        try {
            String firstLine = scanner.readLine();

            if (firstLine == null) {
                throw new InvalidInputFile("Empty file.");
            }

            String[] splitted = firstLine.split("\\s+");
            if (splitted.length > 1) {
                throw new InvalidInputFile("First line should contains only one number.");
            }

            numberOfCouples = Integer.parseInt(firstLine);
        } catch (NumberFormatException e) {
            throw new InvalidInputFile("Cannot parse first line as number.");
        }

        return numberOfCouples;
    }
}
