package hu.bozodavid.minoseg.beadando;

import hu.bozodavid.minoseg.beadando.exceptions.InvalidMatrixValue;

public class IntersectionMatrix {
    private int[][] data;
    private int n;

    /**
     * @param n The matrix size. (Number of couples on the island)
     */
    public IntersectionMatrix(int n) {
        this.data = new int[n][n];
        this.n = n;
    }

    public int get(int huntingField, int farm) {
        return data[huntingField][farm];
    }

    public void set(int huntingField, int farm, int value) throws InvalidMatrixValue {
        if (value < 0) {
            throw new InvalidMatrixValue("Intersection can not be a negative value.");
        }
        data[huntingField][farm] = value;
    }

    public int[] getRow(int huntingField) {
        return data[huntingField];
    }

    public void setRow(int huntingField, int[] values) throws InvalidMatrixValue {
        for (int v : values) {
            if (v < 0) {
                throw new InvalidMatrixValue("Intersection row can not contain negative value.");
            }
        }
        data[huntingField] = values;
    }

    public int getSize() {
        return n;
    }
}
