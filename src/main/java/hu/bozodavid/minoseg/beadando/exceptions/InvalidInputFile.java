package hu.bozodavid.minoseg.beadando.exceptions;

public class InvalidInputFile extends Exception {
    public InvalidInputFile() {
    }

    public InvalidInputFile(String message) {
        super(message);
    }

    public InvalidInputFile(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidInputFile(Throwable cause) {
        super(cause);
    }
}
