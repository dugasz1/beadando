package hu.bozodavid.minoseg.beadando.exceptions;

public class InvalidMatrixValue extends Exception {

    public InvalidMatrixValue(String message) {
        super(message);
    }
}
