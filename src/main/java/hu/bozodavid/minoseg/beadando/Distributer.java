package hu.bozodavid.minoseg.beadando;

public interface Distributer {
    void distribute();
    int getMinIntersection();
    int getRunCount();
}
